package pg.ium.spizhclient.persistence;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.RoomDatabase;

import pg.ium.spizhclient.persistence.dao.ProductPriceDao;
import pg.ium.spizhclient.persistence.dao.QuantityDao;
import pg.ium.spizhclient.persistence.dao.UserDao;
import pg.ium.spizhclient.persistence.dao.UserProductDao;
import pg.ium.spizhclient.persistence.entity.ProductPrice;
import pg.ium.spizhclient.persistence.entity.Quantity;
import pg.ium.spizhclient.persistence.entity.User;
import pg.ium.spizhclient.persistence.entity.UserProduct;

/**
 * Created by MaciejS_ on 2017-12-18.
 */

@Database(entities = {UserProduct.class,
                        Quantity.class,
                        User.class,
                        ProductPrice.class},
        version = 12)
public abstract class AppDB extends RoomDatabase {
    public abstract UserProductDao userProductDao();

    public abstract QuantityDao quantityDao();

    public abstract UserDao userDao();

    public abstract ProductPriceDao productPriceDao();
}

