package pg.ium.spizhclient.persistence.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import java.util.List;

import pg.ium.spizhclient.persistence.entity.Quantity;
import pg.ium.spizhclient.persistence.entity.UserProduct;

/**
 * Created by MaciejS_ on 2017-12-18.
 */

@Dao
public interface QuantityDao {

    @Query("SELECT * FROM quantity WHERE product_uuid = :product_uuid")
    Quantity getQuantityForProduct(String product_uuid);

    @Query("SELECT * FROM quantity")
    List<Quantity> getAll();

    @Update
    void update(Quantity quantity);

    @Insert
    void insert(Quantity quantity);

    @Delete
    void delete(Quantity quantity);
}
