package pg.ium.spizhclient.persistence.entity;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.ForeignKey;
import android.arch.persistence.room.PrimaryKey;

/**
 * Created by MaciejS_ on 2017-12-18.
 */

@Entity(foreignKeys = @ForeignKey(entity = UserProduct.class,
        parentColumns = "uuid",
        childColumns = "product_uuid",
        onDelete = ForeignKey.CASCADE))
public class Quantity {

    public Quantity(String productID, int localQuantity, int remoteQuantity) {
        this.productID = productID;
        this.localQuantity = localQuantity;
        this.remoteQuantity = remoteQuantity;
    }

    @PrimaryKey(autoGenerate = true)
    private int uid;

    @ColumnInfo(name = "product_uuid")
    private String productID;

    @ColumnInfo(name = "local_qty")
    private int localQuantity;

    @ColumnInfo(name = "remote_qty")
    private int remoteQuantity;

    public int getTotalQuantity() {
        return localQuantity + remoteQuantity;
    }

    public int getUid() {
        return uid;
    }

    public String getProductID() {
        return productID;
    }

    public int getLocalQuantity() {
        return localQuantity;
    }

    public int getRemoteQuantity() {
        return remoteQuantity;
    }

    public void setUid(int uid) {
        this.uid = uid;
    }

    public void setProductID(String productID) {
        this.productID = productID;
    }

    public void setLocalQuantity(int localQuantity) {
        this.localQuantity = localQuantity;
    }

    public void setRemoteQuantity(int remoteQuantity) {
        this.remoteQuantity = remoteQuantity;
    }

    public void updateLocalQuantity(int delta) {
        this.localQuantity += delta;
    }

    public String toString(){
        StringBuilder sb = new StringBuilder().append(uid ).append(": ")
                .append(productID + ", ")
                .append(localQuantity).append(", ")
                .append(remoteQuantity).append(", ");
        return sb.toString();
    }

}
