package pg.ium.spizhclient.persistence.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import pg.ium.spizhclient.persistence.entity.User;

/**
 * Created by MaciejS_ on 2017-12-18.
 */

@Dao
public interface UserDao {
    
    @Query("SELECT * FROM user WHERE username = :username")
    User getUserByUsername(String username);

    @Query("SELECT * FROM user WHERE uuid = :uuid")
    User getByUUID(int uuid);

    @Update
    void update(User user);

    @Insert
    void insert(User user);

    @Delete
    void delete(User user);
}
