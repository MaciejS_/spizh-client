package pg.ium.spizhclient.persistence.entity;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.ForeignKey;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

/**
 * Created by MaciejS_ on 2018-01-14.
 */

@Entity(foreignKeys = @ForeignKey(entity = UserProduct.class,
        parentColumns = "uuid",
        childColumns = "user_product_uuid",
        onDelete = ForeignKey.CASCADE))
public class ProductPrice {

    public ProductPrice(String uuid,
                        String userProductUUID,
                        String store,
                        double price,
                        int lastUpdateTimestamp) {
        if (uuid == null) {
            this.uuid = UUID.randomUUID().toString();
        } else {
            this.uuid = uuid;
        }
        this.userProductUUID = userProductUUID;
        this.store = store;
        this.price = price;
        this.lastUpdateTimestamp = lastUpdateTimestamp;
    }

    @PrimaryKey
    @NonNull
    private String uuid;

    @ColumnInfo(name = "user_product_uuid")
    @NonNull
    private String userProductUUID;

    @ColumnInfo(name = "store")
    @NonNull
    private String store;

    @ColumnInfo(name = "price")
    @NonNull
    private Double price;

    @ColumnInfo(name = "last_update_timestamp")
    @NonNull
    private Integer lastUpdateTimestamp;

    @ColumnInfo(name = "is_removed")
    @Nullable
    private Boolean isRemoved;

    @NonNull
    public String getUuid() {
        return uuid;
    }

    public void setUuid(@NonNull String uuid) {
        this.uuid = uuid;
    }

    public String getUserProductUUID() {
        return userProductUUID;
    }

    public void setUserProductUUID(String userProductUUID) {
        this.userProductUUID = userProductUUID;
    }

    public String getStore() {
        return store;
    }

    public void setStore(String store) {
        this.store = store;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public Integer getLastUpdateTimestamp() {
        return lastUpdateTimestamp;
    }

    public void setLastUpdateTimestamp(int lastUpdateTimestamp) {
        this.lastUpdateTimestamp = lastUpdateTimestamp;
    }

    public Boolean getRemoved() {
        if (isRemoved == null){
            return Boolean.FALSE;
        } else {
            return isRemoved;
        }
    }

    public void setRemoved(boolean removed){
        this.isRemoved = removed;
    }

    public void revive() {
        setRemoved(false);
    }

    public Map<String, Object> toMap() {
        Map<String, Object> map = new HashMap<>(4);
        map.put("uuid", uuid);
        map.put("store", store);
        map.put("price", price);
        map.put("userProductUUID", userProductUUID);
        map.put("lastUpdateTimestamp", lastUpdateTimestamp);
        return map;
    }

    public String toString(){
        StringBuilder sb = new StringBuilder().append(uuid + ": ")
                .append(store + ", ")
                .append(price).append(", ")
                .append(userProductUUID + ", ")
                .append(lastUpdateTimestamp);
        return sb.toString();
    }
}
