package pg.ium.spizhclient.persistence.entity;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.ForeignKey;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;


/**
 * Created by MaciejS_ on 2017-12-18.
 */


@Entity(foreignKeys = @ForeignKey(entity = User.class,
        parentColumns = "uuid",
        childColumns = "user_uuid",
        onDelete = ForeignKey.CASCADE))

public class UserProduct {

    public UserProduct(String userUUID,
                       String name,
                       String store,
                       double price,
                       boolean isLocalOnly,
                       int priority,
                       int lastUpdateTimestamp) {
        this.uuid = UUID.randomUUID().toString();
        this.userUUID = userUUID;
        this.name = name;
        this.store = store;
        this.price = price;
        this.isLocalOnly = isLocalOnly;
        this.isRemoved = false;
        this.priority = priority;
        this.lastUpdateTimestamp = lastUpdateTimestamp;
    }

    @PrimaryKey
    @NonNull
    private String uuid;

    @ColumnInfo(name = "user_uuid")
    private String userUUID;

    @ColumnInfo(name = "name")
    private String name;

    @ColumnInfo(name = "store")
    private String store;

    @ColumnInfo(name = "price")
    private double price;

    @ColumnInfo(name = "is_local_only")
    private boolean isLocalOnly;

    @ColumnInfo(name = "is_removed")
    private boolean isRemoved;

    @ColumnInfo(name = "priority")
    private int priority;

    @ColumnInfo(name = "last_update_timestamp")
    private int lastUpdateTimestamp;

    public int getLastUpdateTimestamp() {
        return lastUpdateTimestamp;
    }

    public void setLastUpdateTimestamp(int lastUpdateTimestamp) {
        this.lastUpdateTimestamp = lastUpdateTimestamp;
    }

    public void bumpLastUpdateTime(){
        this.lastUpdateTimestamp = (int)(System.currentTimeMillis() / 1000L);
    }

    public int getPriority() {
        return priority;
    }

    public void setPriority(int priority) {
        this.priority = priority;
    }

    public boolean isRemoved() {
        return isRemoved;
    }

    public String getUuid() {
        return uuid;
    }

    public String getUserUUID() {
        return userUUID;
    }

    public String getName() {
        return name;
    }

    public String getStore() {
        return store;
    }

    public double getPrice() {
        return price;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public void setUserUUID(String userUUID) {
        this.userUUID = userUUID;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setStore(String store) {
        this.store = store;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public boolean isLocalOnly() {
        return isLocalOnly;
    }

    public void setLocalOnly(boolean localOnly) {
        isLocalOnly = localOnly;
    }

    public void setRemoved(boolean removed) {
        isRemoved = removed;
    }

    public Map<String, Object> toMap() {
        Map<String, Object> map = new HashMap<>(4);
        map.put("uuid", uuid);
        map.put("name", name);
        map.put("store", store);
        map.put("price", price);
        map.put("priority", priority);
        map.put("lastUpdateTimestamp", priority);
        return map;
    }

    public String toString(){
        StringBuilder sb = new StringBuilder().append(uuid + ": ")
                .append(name + ", ")
                .append(store + ", ")
                .append(price).append(", ")
                .append(userUUID + ", ")
                .append(isLocalOnly).append(", ")
                .append(isRemoved).append(", ")
                .append(priority).append(", ")
                .append(lastUpdateTimestamp);
        return sb.toString();
    }
}
