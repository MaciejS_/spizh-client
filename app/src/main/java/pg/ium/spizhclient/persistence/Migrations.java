package pg.ium.spizhclient.persistence;

import android.arch.persistence.db.SupportSQLiteDatabase;
import android.arch.persistence.room.migration.Migration;
import android.support.annotation.NonNull;

/**
 * Created by MaciejS_ on 2018-01-13.
 */

public class Migrations {

    public static final Migration migrate5to6 = new Migration(5, 6) {

        @Override
        public void migrate(@NonNull SupportSQLiteDatabase database) {
            database.execSQL("ALTER TABLE userproduct ADD COLUMN priority INTEGER NOT NULL DEFAULT 0;");
//            database.execSQL("UPDATE userproduct SET priority = 0;");
        }
    };

    public static final Migration migrate6to7 = new Migration(6, 7) {
        @Override
        public void migrate(@NonNull SupportSQLiteDatabase database) {
            database.execSQL("ALTER TABLE userproduct ADD COLUMN last_update_timestamp INTEGER NOT NULL DEFAULT 0;");
        }
    };

    public static final Migration migrate7to8 = new Migration(7, 8) {
        @Override
        public void migrate(@NonNull SupportSQLiteDatabase database) {
            // let's leave this out for now, we'll see how room manages that on its own
            database.execSQL(
                    "create table productprice" +
                    "(" +
                    "  uuid TEXT not null primary key DEFAULT (lower(hex(randomblob(4))) || '-' ||" +
                    "                                                   lower(hex(randomblob(2))) || '-' ||" +
                    "                                                   lower(hex(randomblob(2))) || '-' ||" +
                    "                                                   lower(hex(randomblob(2))) || '-' ||" +
                    "                                                   lower(hex(randomblob(6))))," +
                    "   user_product_uuid TEXT not null" +
                    "       references UserProduct (uuid)" +
                    "           on delete cascade," +
                    "   price REAL not null," +
                    "   store TEXT not null," +
                    "   last_update_timestamp INTEGER default 0 not null" +
                    ");"
            );

            database.execSQL("insert into productprice (user_product_uuid, price, store)" +
                                " SELECT uuid, price, store " +
                                " from userproduct ;"
            );
        }
    };

    public static final Migration migrate8to9 = new Migration(8, 9) {
        @Override
        public void migrate(@NonNull SupportSQLiteDatabase database) {
            database.execSQL("ALTER TABLE productprice ADD COLUMN is_removed INTEGER NOT NULL DEFAULT 0;");
        }
    };

    public static final Migration migrate9to10 = new Migration(9, 10) {
        @Override
        public void migrate(@NonNull SupportSQLiteDatabase database) {
            database.execSQL(
                    "create table productprice_tmp" +
                            "(" +
                            "  uuid TEXT not null primary key DEFAULT (lower(hex(randomblob(4))) || '-' ||" +
                            "                                                   lower(hex(randomblob(2))) || '-' ||" +
                            "                                                   lower(hex(randomblob(2))) || '-' ||" +
                            "                                                   lower(hex(randomblob(2))) || '-' ||" +
                            "                                                   lower(hex(randomblob(6))))," +
                            "   user_product_uuid TEXT not null" +
                            "       references UserProduct (uuid)" +
                            "           on delete cascade," +
                            "   price REAL not null," +
                            "   store TEXT not null," +
                            "   last_update_timestamp INTEGER default 0 not null," +
                            "   is_removed INTEGER default 0" +
                            ");"
            );

            database.execSQL("insert into productprice_tmp (uuid, user_product_uuid, price, store, last_update_timestamp, is_removed)" +
                    " SELECT uuid, user_product_uuid, price, store, last_update_timestamp, is_removed" +
                    " from productprice ;"
            );

            database.execSQL("DROP TABLE productprice;");

            database.execSQL(
                    "create table productprice" +
                            "(" +
                            "  uuid TEXT not null primary key DEFAULT (lower(hex(randomblob(4))) || '-' ||" +
                            "                                                   lower(hex(randomblob(2))) || '-' ||" +
                            "                                                   lower(hex(randomblob(2))) || '-' ||" +
                            "                                                   lower(hex(randomblob(2))) || '-' ||" +
                            "                                                   lower(hex(randomblob(6))))," +
                            "   user_product_uuid TEXT not null" +
                            "       references UserProduct (uuid)" +
                            "           on delete cascade," +
                            "   price REAL not null," +
                            "   store TEXT not null," +
                            "   last_update_timestamp INTEGER default 0 not null," +
                            "   is_removed INTEGER default 0" +
                            ");"
            );
            database.execSQL("insert into productprice (uuid, user_product_uuid, price, store, last_update_timestamp, is_removed)" +
                    " SELECT uuid, user_product_uuid, price, store, last_update_timestamp, is_removed" +
                    " from productprice_tmp ;"
            );
            database.execSQL("DROP TABLE productprice_tmp;");
        }
    };

    public static final Migration migrate10to11 = new Migration(10, 11) {
        @Override
        public void migrate(@NonNull SupportSQLiteDatabase database) {
            database.execSQL(
                    "create table productprice_tmp" +
                            "(" +
                            "  uuid TEXT not null primary key DEFAULT (lower(hex(randomblob(4))) || '-' ||" +
                            "                                                   lower(hex(randomblob(2))) || '-' ||" +
                            "                                                   lower(hex(randomblob(2))) || '-' ||" +
                            "                                                   lower(hex(randomblob(2))) || '-' ||" +
                            "                                                   lower(hex(randomblob(6))))," +
                            "   user_product_uuid TEXT not null" +
                            "       references UserProduct (uuid)" +
                            "           on delete cascade," +
                            "   price REAL not null," +
                            "   store TEXT not null," +
                            "   last_update_timestamp INTEGER default 0 not null," +
                            "   is_removed INTEGER default 0" +
                            ");"
            );

            database.execSQL("insert into productprice_tmp (uuid, user_product_uuid, price, store, last_update_timestamp, is_removed)" +
                    " SELECT uuid, user_product_uuid, price, store, last_update_timestamp, is_removed" +
                    " from productprice ;"
            );

            database.execSQL("DROP TABLE productprice;");

            database.execSQL(
                    "create table productprice" +
                            "(" +
                            "  uuid TEXT not null primary key DEFAULT (lower(hex(randomblob(4))) || '-' ||" +
                            "                                                   lower(hex(randomblob(2))) || '-' ||" +
                            "                                                   lower(hex(randomblob(2))) || '-' ||" +
                            "                                                   lower(hex(randomblob(2))) || '-' ||" +
                            "                                                   lower(hex(randomblob(6))))," +
                            "   user_product_uuid TEXT not null" +
                            "       references UserProduct (uuid)" +
                            "           on delete cascade," +
                            "   price REAL not null," +
                            "   store TEXT not null," +
                            "   last_update_timestamp INTEGER default 0 not null," +
                            "   is_removed INTEGER default 0" +
                            ");"
            );
            database.execSQL("insert into productprice (uuid, user_product_uuid, price, store, last_update_timestamp, is_removed)" +
                    " SELECT uuid, user_product_uuid, price, store, last_update_timestamp, is_removed" +
                    " from productprice_tmp ;"
            );
            database.execSQL("DROP TABLE productprice_tmp;");
        }
    };

    public static final Migration migrate11to12 = new Migration(11, 12) {
        @Override
        public void migrate(@NonNull SupportSQLiteDatabase database) {
            database.execSQL("DELETE FROM productprice WHERE is_removed IS NULL;");
        }
    };
}
