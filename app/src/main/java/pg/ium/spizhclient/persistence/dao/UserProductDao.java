package pg.ium.spizhclient.persistence.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import java.util.Collection;
import java.util.List;

import pg.ium.spizhclient.persistence.entity.UserProduct;

/**
 * Created by MaciejS_ on 2017-12-18.
 */

@Dao
public interface UserProductDao {

    @Query("SELECT * FROM userproduct WHERE user_uuid = :user_id")
    List<UserProduct> getAllForUser(String user_id);

    @Query("SELECT * FROM userproduct WHERE user_uuid = :user_id AND is_removed = 0")
    List<UserProduct> getDisplayableProductsForUser(String user_id);

    @Query("SELECT * FROM userproduct WHERE uuid = :uuid")
    UserProduct getByUUID(String uuid);

    @Query("SELECT * FROM userproduct WHERE user_uuid = :user_id AND is_local_only = 1 AND is_removed = 0")
    List<UserProduct> getProductsForAddSync(String user_id);

    @Query("SELECT * FROM userproduct WHERE user_uuid = :user_id AND is_removed = 1")
    List<UserProduct> getProductsForRemoveSync(String user_id);

    @Update
    void update(UserProduct userProduct);

    @Insert
    void insert(UserProduct userProduct);

    @Delete
    void delete(UserProduct userProduct);

    @Delete
    void deleteAll(Collection<UserProduct> userProductCollection);
}
