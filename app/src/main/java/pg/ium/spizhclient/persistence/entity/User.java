package pg.ium.spizhclient.persistence.entity;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;


/**
 * Created by MaciejS_ on 2017-12-18.
 */

@Entity
public class User {

    public User(String uuid, String username, String auth) {
        this.uuid = uuid;
        this.username = username;
        this.auth = auth;
    }

    @PrimaryKey
    @NonNull
    private String uuid;

    @ColumnInfo(name = "username")
    private String username;

    @ColumnInfo(name = "auth")
    private String auth;

    public String getUuid() {
        return uuid;
    }

    public String getUsername() {
        return username;
    }

    public String getAuth() {
        return auth;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void setAuth(String auth) {
        this.auth = auth;
    }
}
