package pg.ium.spizhclient.persistence.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import java.util.Collection;
import java.util.List;

import pg.ium.spizhclient.persistence.entity.ProductPrice;

/**
 * Created by MaciejS_ on 2018-01-14.
 */

@Dao
public interface ProductPriceDao {

    @Query("SELECT * FROM productprice WHERE user_product_uuid = :userProductUUID")
    List<ProductPrice> getPricesForProduct(String userProductUUID);

    @Query("SELECT * FROM productprice WHERE user_product_uuid = :userProductUUID AND is_removed = 0")
    List<ProductPrice> getVisiblePricesForProduct(String userProductUUID);

    @Query("SELECT * FROM productprice WHERE uuid = :productPriceUUID")
    ProductPrice getPrice(String productPriceUUID);

    @Update
    void update(ProductPrice productPrice);

    @Insert
    void insert(ProductPrice productPrice);

    @Delete
    void delete(ProductPrice productPrice);

    @Delete
    void deleteAll(Collection<ProductPrice> productPriceCollection);
}
