package pg.ium.spizhclient;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import pg.ium.spizhclient.persistence.entity.ProductPrice;

/**
 * Created by MaciejS_ on 2017-11-13.
 */

class Product {

    public Product(String id,
                   String name,
                   int quantity,
                   int priority,
                   List<ProductPrice> prices) {
        this.id = id;
        this.name = name;
        this.quantity = quantity;
        this.priority = priority;
        this.prices = prices;
    }

    public Product(Product product) {
        // TODO: modifications made to String fields might carry over to the original object
        this.id = product.id;
        this.name = product.getName();
        this.quantity = product.getQuantity();
        this.priority = product.getPriority();
        this.prices = product.getPrices();
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public List<ProductPrice> getPrices() {
        return prices;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public void incrementQuantity(int delta) {
        this.quantity += delta;
    }

    public int getPriority() {
        return priority;
    }

    public void setPriority(int priority) {
        this.priority = priority;
    }

    @Override
    public String toString() {
        return "Product{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", quantity=" + quantity +
                ", priority=" + priority +
                '}';
    }

    public Map<String, Object> toMap() {
        Map<String, Object> map = new HashMap<>(4);
        map.put("name", name);
        map.put("quantity", quantity);
        map.put("priority", priority);
        return map;
    }

    private String name;
    private int quantity;
    private String id;
    private int priority;
    private List<ProductPrice> prices;
}
