package pg.ium.spizhclient.sync;

import android.support.annotation.NonNull;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import pg.ium.spizhclient.persistence.AppDB;
import pg.ium.spizhclient.persistence.entity.ProductPrice;
import pg.ium.spizhclient.persistence.entity.Quantity;
import pg.ium.spizhclient.persistence.entity.UserProduct;

/**
 * Created by MaciejS_ on 2018-01-15.
 */

public class OutgoingSyncPackaging {
    public static JSONArray assemblePricesUpdate(AppDB appDB, String userID) {
        JSONArray products_update = new JSONArray();
        for (UserProduct up : appDB.userProductDao().getDisplayableProductsForUser(userID)) {
            for (ProductPrice price : appDB.productPriceDao().getPricesForProduct(up.getUuid())) {
                JSONObject priceJson = priceToJSON(price);
                products_update.put(priceJson);
            }
        }
        return products_update;
    }

    @NonNull
    public static JSONObject assembleQuantitiesUpdate(AppDB appDB, String userID) {
        JSONObject qty_updates = new JSONObject();
        for (UserProduct up : appDB.userProductDao().getAllForUser(userID)) {
            String productID = up.getUuid();
            Quantity q = appDB.quantityDao().getQuantityForProduct(productID);
            int qty;
            if (q == null) {
                qty = 0;
            } else {
                qty = q.getLocalQuantity();
            }
            try {
                qty_updates.put(productID, qty);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return qty_updates;
    }

    @NonNull
    public static JSONArray assembleAddedProductsUpdate(AppDB appDB, String userID) {
        JSONArray products_update = new JSONArray();
        for (UserProduct up : appDB.userProductDao().getProductsForAddSync(userID)) {
            JSONObject productJson = productToJSON(up);
            products_update.put(productJson);
        }
        return products_update;
    }

    @NonNull
    public static JSONArray assembleUpdatedProductsUpdate(AppDB appDB, String userID) {
        JSONArray products_update = new JSONArray();
        for (UserProduct up : appDB.userProductDao().getDisplayableProductsForUser(userID)) {
            JSONObject productJson = productToJSON(up);
            products_update.put(productJson);
        }
        return products_update;
    }

    @NonNull
    public static JSONArray assembleRemovedProductsBundle(AppDB appDB, String userID) {
        JSONArray removedProductIDs = new JSONArray();
        for (UserProduct up : appDB.userProductDao().getProductsForRemoveSync(userID)) {
            removedProductIDs.put(up.getUuid());
        }
        return removedProductIDs;
    }

    @NonNull
    private static JSONObject productToJSON(UserProduct up) {
        JSONObject productJson = new JSONObject();
        try {
            productJson.put("name", up.getName());
            productJson.put("store", up.getStore());
            productJson.put("price", up.getPrice());
            productJson.put("uuid", up.getUuid());
            productJson.put("priority", up.getPriority());
            productJson.put("last_update_timestamp", up.getLastUpdateTimestamp());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return productJson;
    }

    @NonNull
    private static JSONObject priceToJSON(ProductPrice pp) {
        JSONObject productJson = new JSONObject();
        try {
            productJson.put("uuid", pp.getUuid());
            productJson.put("store", pp.getStore());
            productJson.put("price", pp.getPrice());
            productJson.put("user_product_uuid", pp.getUserProductUUID());
            productJson.put("last_update_timestamp", pp.getLastUpdateTimestamp());
            Boolean removed = pp.getRemoved();
            if (removed != null){
                productJson.put("is_removed", removed);
            } else {
                productJson.put("is_removed", Boolean.FALSE);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return productJson;
    }
}
