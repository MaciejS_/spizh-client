package pg.ium.spizhclient.sync;

import android.database.sqlite.SQLiteConstraintException;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;
import java.util.Map;

import pg.ium.spizhclient.persistence.AppDB;
import pg.ium.spizhclient.persistence.dao.ProductPriceDao;
import pg.ium.spizhclient.persistence.entity.ProductPrice;
import pg.ium.spizhclient.persistence.entity.Quantity;
import pg.ium.spizhclient.persistence.entity.UserProduct;

/**
 * Created by MaciejS_ on 2018-01-15.
 */

public class IncomingSyncLogic {
    public static void applyUpdate(JSONObject serverResponse, AppDB appDB, String userID) {
        try {
            JSONObject productUpdates = serverResponse.getJSONObject("product");
            List<UserProduct> addedProducts = IncomingSyncUnwrapping.unwrapAddedProductsBundle(userID,
                    productUpdates.getJSONArray("added"));
            List<UserProduct> updatedProducts = IncomingSyncUnwrapping.unwrapUpdatedProductsBundle(userID, productUpdates.getJSONArray("updated"));
            List<String> removedProducts = IncomingSyncUnwrapping.unwrapListOfProductsToRemove(productUpdates.getJSONArray("removed"));
            Map<String, Integer> quantity_updates = IncomingSyncUnwrapping.unwrapListOfQuantityUpdates(serverResponse.getJSONObject("quantity"));
            List<ProductPrice> productPrices = IncomingSyncUnwrapping.unwrapListOfProductPrices(serverResponse.getJSONArray("price"));

            applyAddedProductsUpdates(appDB, addedProducts);
            applyProductPropertiesUpdates(appDB, updatedProducts);
            applyRemoveProducts(appDB, removedProducts);
            applyQuantityUpdates(appDB, quantity_updates);
            applyPriceUpdates(appDB, productPrices);

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private static void applyPriceUpdates(AppDB appDB, List<ProductPrice> productPrices) {
        ProductPriceDao productPriceDao = appDB.productPriceDao();
        for (int i = 0; i < productPrices.size(); i++) {
            ProductPrice pp = productPrices.get(i);
            ProductPrice existingProductPrice = productPriceDao.getPrice(pp.getUuid());
            if (existingProductPrice == null){
                try {
                    Log.i("PRICE UPDATE NEW", pp.toString());
                    productPriceDao.insert(pp);
                }catch (SQLiteConstraintException e){
                    Log.e("PRICE UPDATE NEW EXC", e.toString());
                }
            } else {
                if (existingProductPrice.getLastUpdateTimestamp() < pp.getLastUpdateTimestamp()){
                    productPriceDao.update(pp);
                    Log.i("PRICE UPDATE QTY EXIST", pp.toString());
                }
            }
        }
    }

    private static void applyQuantityUpdates(AppDB appDB, Map<String, Integer> quantity_updates) {
        for (Map.Entry<String, Integer> entry : quantity_updates.entrySet()) {
            Log.i("QTY UPDATE ATTEMPT", entry.toString());
            Quantity q = appDB.quantityDao().getQuantityForProduct(entry.getKey());
            if (q != null){
                Log.i("QTY UPDATE NOT NULL", "before " + q.toString());
                q.setRemoteQuantity(entry.getValue());
                Log.i("QTY UPDATE NOT NULL", "after " + q.toString());
                appDB.quantityDao().update(q);
            } else {
                q = new Quantity(entry.getKey(), 0, entry.getValue());
                Log.i("QTY UPDATE NULL", "after " + q.toString());
                appDB.quantityDao().insert(q);
            }
        }
    }

    private static void applyRemoveProducts(AppDB appDB, List<String> removedProducts) {
        for(String productUUID: removedProducts){
            UserProduct p = appDB.userProductDao().getByUUID(productUUID);
            p.setRemoved(true);
            appDB.userProductDao().update(p);
        }
    }

    private static void applyProductPropertiesUpdates(AppDB appDB, List<UserProduct> updatedProducts) {
        for(UserProduct up: updatedProducts){
            try {
                UserProduct inDBUserProduct = appDB.userProductDao().getByUUID(up.getUuid());
                if (inDBUserProduct != null) {

                    Log.i("UPDATEPRODUCT inDB:", inDBUserProduct.toString());
                    Log.i("UPDATEPRODUCT external:", up.toString());
                    if (inDBUserProduct.getLastUpdateTimestamp() < up.getLastUpdateTimestamp()){
                        Log.i("UPDATED PRODUCT UPDATE", "--Updating...--");
                        inDBUserProduct.setPriority(up.getPriority());
                        inDBUserProduct.setName(up.getName());
                        inDBUserProduct.setPrice(up.getPrice());
                        inDBUserProduct.setStore(up.getStore());
                        appDB.userProductDao().update(up);
                    }
                    else{
                        Log.i("UPDATED PRODUCT UPDATE", "--Not updating--");
                    }
                    Log.i("UPDATED PRODUCT UPDATE", "-----------");
                } else {
//                        appDB.userProductDao().insert(up);
                    Log.e("UPDATED PRODUCT UPDATE", String.format("%s doesn't exist but is in update bundle (%s)", up.getName(), up.getUuid()));
                }
            } catch (Exception e){
                // the goal is to catch exceptions that might arise if target row doesn't exist
                e.printStackTrace();
            }
        }
    }

    private static void applyAddedProductsUpdates(AppDB appDB, List<UserProduct> addedProducts) {
        for(UserProduct up : addedProducts){
            try {
                if (appDB.userProductDao().getByUUID(up.getUuid()) == null) {
                    appDB.userProductDao().insert(up);
                } else {
                    Log.e("ADDED PRODUCT UPDATE", String.format("%s already exists (%s)", up.getName(), up.getUuid()));
                }
            } catch (Exception e){
                // the goal is to catch exceptions that might arise if target row doesn't exist
                e.printStackTrace();
            }
        }
    }
}
