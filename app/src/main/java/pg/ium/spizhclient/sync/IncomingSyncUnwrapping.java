package pg.ium.spizhclient.sync;

import android.support.annotation.NonNull;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import pg.ium.spizhclient.persistence.entity.ProductPrice;
import pg.ium.spizhclient.persistence.entity.UserProduct;

/**
 * Created by MaciejS_ on 2018-01-15.
 */

public class IncomingSyncUnwrapping {
    public static List<ProductPrice> unwrapListOfProductPrices(JSONArray pricesList) {
        List<ProductPrice> prices = new ArrayList<>();
        for (int i = 0; i < pricesList.length(); i++) {
            try {
                prices.add(deserializePrice((JSONObject) pricesList.get(i)));

            } catch (JSONException e ){
                e.printStackTrace();
                Log.e("PRICE BUNDLE LOAD", "Price deserialization failed");
            }
        }
        return prices;
    }

    public static ProductPrice deserializePrice(JSONObject json) throws JSONException {
        ProductPrice pp = new ProductPrice(json.getString("uuid"),
                json.getString("user_product_uuid"),
                json.getString("store"),
                json.getDouble("price"),
                json.getInt("last_update_timestamp"));
        return pp;
    }

    public static List<UserProduct> unwrapAddedProductsBundle(String userUUID,
                                                              JSONArray addedProductsBundle) {
        // name, store, price
        List<UserProduct> userProducts = new ArrayList<>();
        for (int i = 0; i < addedProductsBundle.length(); i++) {
            try {
                JSONObject newProductJSON = addedProductsBundle.getJSONObject(i);
                UserProduct up = productJsonToProduct(userUUID, newProductJSON, i);
                userProducts.add(up);
            } catch (JSONException e) {
                e.printStackTrace();
                Log.e("ADDED BUNDLE LOAD", "Product deserialization failed");
            }
        }
        return userProducts;
    }

    @NonNull
    public static UserProduct productJsonToProduct(String userUUID, JSONObject productJSON, int i) throws JSONException {
        UserProduct up = new UserProduct(userUUID,
                productJSON.getString("name"),
                productJSON.getString("store"),
                productJSON.getDouble("price"),
                false,
                productJSON.getInt("priority"),
                productJSON.getInt("last_update_timestamp"));
        up.setUuid(productJSON.getString("uuid"));
        return up;
    }

    public static List<UserProduct> unwrapUpdatedProductsBundle(String userUUID, JSONArray updatedProductsBundle){
        // name, store, price
        List<UserProduct> userProducts = new ArrayList<>();
        for (int i = 0; i < updatedProductsBundle.length(); i++) {
            try {
                JSONObject productJSON = updatedProductsBundle.getJSONObject(i);
                UserProduct up = productJsonToProduct(userUUID, productJSON, i);
                userProducts.add(up);
            } catch (JSONException e) {
                e.printStackTrace();
                Log.e("ADDED BUNDLE LOAD", "Product deserialization failed");
            }
        }
        return userProducts;
    }

    public static List<String> unwrapListOfProductsToRemove(JSONArray productsToRemoveUpdateBundle){
        List<String> productsToRemove = new ArrayList<>();
        for (int i = 0; i < productsToRemoveUpdateBundle.length(); i++){
            try {
                productsToRemove.add(productsToRemoveUpdateBundle.getString(i));
            } catch (JSONException e) {
                e.printStackTrace();
                Log.e("REMOVED BUNDLE LOAD", "UUID deserialization failed");
            }
        }
        return productsToRemove;
    }

    public static Map<String, Integer> unwrapListOfQuantityUpdates(JSONObject quantityUpdatesBundle){
        Map<String, Integer> quantityUpdatesMap = new HashMap<>();
        for (Iterator<String> it = quantityUpdatesBundle.keys(); it.hasNext(); ) {
            String key = it.next();
            try {
                quantityUpdatesMap.put(key, quantityUpdatesBundle.getInt(key));
            } catch (JSONException e) {
                e.printStackTrace();
                Log.e("QUANTITY BUNDLE LOAD", "Quantity deserialization failed");
            }
        }
        return quantityUpdatesMap;
    }
}
