package pg.ium.spizhclient;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.arch.persistence.db.SupportSQLiteDatabase;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.migration.Migration;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;

import android.os.AsyncTask;

import android.os.Build;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.EditorInfo;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.NetworkResponse;
import com.android.volley.Response;
import com.android.volley.VolleyError;

import org.json.JSONException;
import org.json.JSONObject;

import pg.ium.spizhclient.persistence.AppDB;
import pg.ium.spizhclient.persistence.Migrations;
import pg.ium.spizhclient.persistence.entity.User;

import static pg.ium.spizhclient.persistence.Migrations.migrate10to11;
import static pg.ium.spizhclient.persistence.Migrations.migrate11to12;
import static pg.ium.spizhclient.persistence.Migrations.migrate8to9;
import static pg.ium.spizhclient.persistence.Migrations.migrate9to10;

/**
 * A login screen that offers login via email/password.
 */
public class LoginActivity extends AppCompatActivity {

    public static final String SPIZH_DB = "spizh.db";
    private UserLoginTask mAuthTask = null;

    // UI references.
    private AutoCompleteTextView mEmailView;
    private EditText mPasswordView;
    private View mProgressView;
    private View mLoginFormView;
    private View mTextView;

    private Server serverHandle;

    AppDB appDb = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        // Set up the login form.
        mEmailView = (AutoCompleteTextView) findViewById(R.id.email);

        mPasswordView = (EditText) findViewById(R.id.password);
        mPasswordView.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int id, KeyEvent keyEvent) {
                if (id == R.id.login || id == EditorInfo.IME_NULL) {
                    attemptLogin();
                    return true;
                }
                return false;
            }
        });

        appDb = Room.databaseBuilder(getApplicationContext(), AppDB.class, SPIZH_DB)
                .allowMainThreadQueries()
                .addMigrations(Migrations.migrate5to6, Migrations.migrate6to7, Migrations.migrate7to8, migrate8to9, migrate9to10, migrate10to11, migrate11to12)
                .build();

        String currentDBPath = getDatabasePath(SPIZH_DB).getAbsolutePath();
        Log.i("DB PATH", currentDBPath);

        Button mEmailSignInButton = (Button) findViewById(R.id.email_sign_in_button);
        mEmailSignInButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                attemptLogin();
            }
        });

        mLoginFormView = findViewById(R.id.login_form);
        mProgressView = findViewById(R.id.login_progress);

        this.serverHandle = new Server(getString(R.string.server_ip_addr), 7447, null, null, getApplicationContext());

        attemptLogin();
    }

    /**
     * Attempts to sign in or register the account specified by the login form.
     * If there are form errors (invalid email, missing fields, etc.), the
     * errors are presented and no actual login attempt is made.
     */
    private void attemptLogin() {
        if (mAuthTask != null) {
            return;
        }

        // Reset errors.
        mEmailView.setError(null);
        mPasswordView.setError(null);

        // Store values at the time of the login attempt.
        String email = mEmailView.getText().toString();
        String password = mPasswordView.getText().toString();

        boolean cancel = false;
        View focusView = null;

        // Check for a valid password, if the user entered one.
        if (!TextUtils.isEmpty(password) && !isPasswordValid(password)) {
            mPasswordView.setError(getString(R.string.error_invalid_password));
            focusView = mPasswordView;
            cancel = true;
        }

        // Check for a valid email address.
        if (TextUtils.isEmpty(email)) {
            mEmailView.setError(getString(R.string.error_field_required));
            focusView = mEmailView;
            cancel = true;
        } else if (!isEmailValid(email)) {
            mEmailView.setError(getString(R.string.error_invalid_email));
            focusView = mEmailView;
            cancel = true;
        }

        if (cancel) {
            // There was an error; don't attempt login and focus the first
            // form field with an error.
            focusView.requestFocus();
        } else {
            // Show a progress spinner, and kick off a background task to
            // perform the user login attempt.
            showProgress(true);
            mAuthTask = new UserLoginTask(email, password);
            mAuthTask.execute((Void) null);
        }
    }

    private boolean isEmailValid(String email) {
        return email.length() > 0;
    }

    private boolean isPasswordValid(String password) {
        return password.length() > 0;
    }

    /**
     * Shows the progress UI and hides the login form.
     */
    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    private void showProgress(final boolean show) {
        // On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
        // for very easy animations. If available, use these APIs to fade-in
        // the progress spinner.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);

            mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
            mLoginFormView.animate().setDuration(shortAnimTime).alpha(
                    show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
                }
            });

            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mProgressView.animate().setDuration(shortAnimTime).alpha(
                    show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
                }
            });
        } else {
            // The ViewPropertyAnimator APIs are not available, so simply show
            // and hide the relevant UI components.
            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
        }
    }

    /**
     * Represents an asynchronous login/registration task used to authenticate
     * the user.
     */
    public class UserLoginTask extends AsyncTask<Void, Void, Boolean> {

        private final String mEmail;
        private final String mPassword;

        UserLoginTask(String email, String password) {
            mEmail = email;
            mPassword = password;
        }

        @Override
        protected Boolean doInBackground(Void... params) {

            mEmailView.setError(null);
            mPasswordView.setError(null);

            Log.d("mEmail", mEmail);
            Log.d("mPassword", mPassword);

            final Boolean[] userExists = {false};

            serverHandle.loginUser(mEmail, mPassword,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            User user = null;
                            String user_id = null;
                            try {
                                user = appDb.userDao().getUserByUsername(mEmail);
                                if (user == null) {
                                    user_id = getUserIDFromServerResponse(new JSONObject(response));
                                    Log.i("USER INSERT", "Attempt to insert user, server resp: " + response.toString());
                                    appDb.userDao().insert(new User(user_id, mEmail, mPassword));
                                    Log.i("USER INSERTED", String.format("User %s inserted with ID(%s) and pwd(%s) after succesful login", mEmail, user_id, mPassword));
                                } else {
                                    user_id = user.getUuid();
                                }
                                serverHandle.setCredentials(mEmail, mPassword);
                                registerDevice(user_id);
                                startProductListActivity();
                            } catch (Exception ex) {
                                Log.e("EXCEPTION A", ex.toString());
                                Log.e("EXCEPTION B", ex.getMessage());
                                Log.e("EXCEPTION C", ex.getCause().toString());
                            }
                            finish();
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            handleLoginError(error);
                        }
                    });
            return true;
        }

        private void startProductListActivity() {
            Intent i = new Intent(getApplicationContext(), ProductListActivity.class);
            i.putExtra("username", mEmail);
            i.putExtra("password", mPassword);
            i.putExtra("user_id", appDb.userDao().getUserByUsername(mEmail).getUuid());
            startActivity(i);
        }

        private void handleLoginError(VolleyError error) {
            Log.d("onError", "handleLoginError entered");
            showProgress(false);
            try {
                NetworkResponse nr = error.networkResponse;
                int sc = nr.statusCode;
                if (sc == 404) {
                    Log.d("onError: 404", "entered 404 block");
                    if (isEmailValid(mEmail) && isPasswordValid(mPassword))
                        registerUser();
                } else if (error.networkResponse.statusCode == 401) {
                    Log.d("onError: 401", "entered 401 block");
                    mPasswordView.setError(getString(R.string.error_incorrect_password));
                    mPasswordView.requestFocus();
                }
            } catch (NullPointerException ex) {
                Log.i("OFFLINE LOGIN", "Offline login attempt entered");

                User user = appDb.userDao().getUserByUsername(mEmail);
                if (user != null) {
                    // user exists in our database - log him in in offline mode
                    Log.i("LOGIN SUCCEEDED", "Offline login succeeded - as " + user.getUsername());

                    Toast t = Toast.makeText(getApplicationContext(),
                            "Logged in in offline mode.",
                            Toast.LENGTH_LONG);
                    t.show();
                    startProductListActivity();
                } else {
                    Log.i("LOGIN FAILED", "Offline login failed");
                    Toast t = Toast.makeText(getApplicationContext(),
                            "Unable to create account in offline mode, try again with internet connection.",
                            Toast.LENGTH_LONG);
                    t.show();
                }

            }
        }

        private void registerUser() {
            AlertDialog registerUserDialog = buildRegisterUserDialog();
            registerUserDialog.show();
        }

        @NonNull
        private View prepareRegisterUserPopup() {
            LayoutInflater popupLayoutInflater = (LayoutInflater) getApplicationContext()
                    .getSystemService(LAYOUT_INFLATER_SERVICE);
            final View addProductDialogView = popupLayoutInflater.inflate(R.layout.register_popup, null);

            final EditText passwordEdit = (EditText) addProductDialogView.findViewById(R.id.password_confirm_edit);
            return addProductDialogView;
        }

        private AlertDialog buildRegisterUserDialog() {
            final View addUserDialogView = prepareRegisterUserPopup();

            AlertDialog.Builder b = new AlertDialog.Builder(LoginActivity.this)
                    .setTitle("Create new account")
                    .setView(addUserDialogView)
                    .setCancelable(false)
                    .setPositiveButton("Register", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            EditText passwordEdit = (EditText) addUserDialogView.findViewById(R.id.password_confirm_edit);
                            if (passwordEdit.getError() == null) {
                                serverHandle.createUser(mEmail, mPassword, new Response.Listener() {
                                    @Override
                                    public void onResponse(Object response) {
                                        Log.i("REGISTER USER", "attempt to parse JSON " + response.toString());
                                        String userId = getUserIDFromServerResponse((JSONObject) response);

                                        appDb.userDao().insert(new User(userId, mEmail, mPassword));
                                        Log.i("USER INSERTED", "User " + mEmail + " inserted after registration");
                                        startProductListActivity();
                                    }
                                }, new Response.ErrorListener() {
                                    @Override
                                    public void onErrorResponse(VolleyError error) {
                                        Toast.makeText(getApplicationContext(), "Server error.", Toast.LENGTH_LONG).show();
                                    }
                                });
                            }
                        }
                    }).setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
            final AlertDialog dialog = b.create();

            final EditText passwordEdit = (EditText) addUserDialogView.findViewById(R.id.password_confirm_edit);
            TextWatcher passwordWatcher = new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {

                }

                @Override
                public void afterTextChanged(Editable s) {
                    Log.d("onTextChanged", s.toString());
                    if (s.length() > 0) {
                        if (!s.toString().equals(mPassword)) {
                            passwordEdit.setError("Passwords don't match");
                            dialog.getButton(AlertDialog.BUTTON_POSITIVE).setEnabled(false);
                        } else {
                            passwordEdit.setError(null);
                            dialog.getButton(AlertDialog.BUTTON_POSITIVE).setEnabled(true);
                        }
                    }
                }
            };
            passwordEdit.addTextChangedListener(passwordWatcher);

            return dialog;
        }

        @Override
        protected void onPostExecute(final Boolean success) {
            mAuthTask = null;
            showProgress(true);
        }

        @Override
        protected void onCancelled() {
            mAuthTask = null;
            showProgress(false);
        }
    }

    private void registerDevice(String userUUID) {
        serverHandle.registerDevice(userUUID,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.i("REGISTER DEVICE", "Device registered successfully");
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.e("DEVICE REGISTER ERROR", error.toString());
                        NetworkResponse nr = error.networkResponse;
                        try {
                            int statusCode = nr.statusCode;
                            if (statusCode == 409) {
                                Log.i("REGISTER DEVICE", "Device already registered!");
                            } else {
                                Log.e("REGISTER DEVICE", "Error " + String.valueOf(statusCode));
                            }
                        } catch (NullPointerException ex) {
                            Toast.makeText(getApplicationContext(), "Unable to register device", Toast.LENGTH_LONG);
                        }
                    }
                });
    }

    private String getUserIDFromServerResponse(JSONObject response) {
        JSONObject responseJson = response;
        String userID = null;

        try {
            userID = responseJson.getString("userid");
            Log.i("GOT USER ID", "UserID is " + userID);

        } catch (JSONException e) {
            e.printStackTrace();
            Log.e("EXCEPTION", "Unable to get user id from reponse json");
        }
        return userID;
    }
}

