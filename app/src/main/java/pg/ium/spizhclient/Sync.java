package pg.ium.spizhclient;

import android.support.annotation.NonNull;
import android.util.Base64;
import android.util.Log;

import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import pg.ium.spizhclient.persistence.AppDB;
import pg.ium.spizhclient.persistence.dao.ProductPriceDao;
import pg.ium.spizhclient.persistence.entity.ProductPrice;
import pg.ium.spizhclient.persistence.entity.Quantity;
import pg.ium.spizhclient.persistence.entity.UserProduct;

/**
 * Created by MaciejS_ on 2017-11-13.
 */

public class Sync {


    public static Map<String, Product> deserializeProductsListJSON(JSONObject response) {
        Log.i("Products raw JSON", response.toString());
        Map<String, Product> productMap = new HashMap<>();

        Gson gson = new Gson();
        Iterator<String> keyIterator = response.keys();
        while (keyIterator.hasNext()) {
            String productId = keyIterator.next();
            try {
                Log.d("JSON Deserialization", "Attempting deserialization of product id:" + productId + "failed to deserialize");
                Product deserializedProduct = gson.fromJson(response.get(productId).toString(), Product.class);
                deserializedProduct.setId(productId);
                productMap.put(productId, deserializedProduct);
                Log.d("JSON Deserialization", "Porduct id:" + productId + " deserialized");
                Log.d("JSON Deserialization", "Porduct id:" + deserializedProduct.toString());

            } catch (JSONException e) {
                Log.e("JSON Deserialization", "Product id:" + productId + " failed to deserialize");
            }
        }
        return productMap;
    }

    public static void applyUpdate(JSONObject serverResponse, AppDB appDB, String userID) {
        try {
            JSONObject productUpdates = serverResponse.getJSONObject("product");
            List<UserProduct> addedProducts = unwrapAddedProductsBundle(userID,
                    productUpdates.getJSONArray("added"));
            List<UserProduct> updatedProducts = unwrapUpdatedProductsBundle(userID, productUpdates.getJSONArray("updated"));
            List<String> removedProducts = unwrapListOfProductsToRemove(productUpdates.getJSONArray("removed"));
            Map<String, Integer> quantity_updates = unwrapListOfQuantityUpdates(serverResponse.getJSONObject("quantity"));
            List<ProductPrice> productPrices = unwrapListOfProductPrices(serverResponse.getJSONArray("price"));

            applyAddedProductsUpdates(appDB, addedProducts);
            applyProductPropertiesUpdates(appDB, updatedProducts);
            applyRemoveProducts(appDB, removedProducts);
            applyQuantityUpdates(appDB, quantity_updates);
            applyPriceUpdates(appDB, productPrices);

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private static void applyPriceUpdates(AppDB appDB, List<ProductPrice> productPrices) {
        ProductPriceDao productPriceDao = appDB.productPriceDao();
        for (int i = 0; i < productPrices.size(); i++) {
            ProductPrice pp = productPrices.get(i);
            ProductPrice existingProductPrice = productPriceDao.getPrice(pp.getUuid());
            if (existingProductPrice == null){
                productPriceDao.insert(pp);
            } else {
                if (existingProductPrice.getLastUpdateTimestamp() < pp.getLastUpdateTimestamp()){
                    productPriceDao.update(pp);
                }
            }
        }
    }

    private static void applyQuantityUpdates(AppDB appDB, Map<String, Integer> quantity_updates) {
        for (Map.Entry<String, Integer> entry : quantity_updates.entrySet()) {
            Log.i("QTY UPDATE ATTEMPT", entry.toString());
            Quantity q = appDB.quantityDao().getQuantityForProduct(entry.getKey());
            if (q != null){
                Log.i("QTY UPDATE NOT NULL", "before " + q.toString());
                q.setRemoteQuantity(entry.getValue());
                Log.i("QTY UPDATE NOT NULL", "after " + q.toString());
                appDB.quantityDao().update(q);
            } else {
                q = new Quantity(entry.getKey(), 0, entry.getValue());
                Log.i("QTY UPDATE NULL", "after " + q.toString());
                appDB.quantityDao().insert(q);
            }
        }
    }

    private static void applyRemoveProducts(AppDB appDB, List<String> removedProducts) {
        for(String productUUID: removedProducts){
            UserProduct p = appDB.userProductDao().getByUUID(productUUID);
            p.setRemoved(true);
            appDB.userProductDao().update(p);
        }
    }

    private static void applyProductPropertiesUpdates(AppDB appDB, List<UserProduct> updatedProducts) {
        for(UserProduct up: updatedProducts){
            try {
                UserProduct inDBUserProduct = appDB.userProductDao().getByUUID(up.getUuid());
                if (inDBUserProduct != null) {

                    Log.i("UPDATEPRODUCT inDB:", inDBUserProduct.toString());
                    Log.i("UPDATEPRODUCT external:", up.toString());
                    if (inDBUserProduct.getLastUpdateTimestamp() < up.getLastUpdateTimestamp()){
                        Log.i("UPDATED PRODUCT UPDATE", "--Updating...--");
                        inDBUserProduct.setPriority(up.getPriority());
                        inDBUserProduct.setName(up.getName());
                        inDBUserProduct.setPrice(up.getPrice());
                        inDBUserProduct.setStore(up.getStore());
                        appDB.userProductDao().update(up);
                    }
                    else{
                        Log.i("UPDATED PRODUCT UPDATE", "--Not updating--");
                    }
                    Log.i("UPDATED PRODUCT UPDATE", "-----------");
                } else {
//                        appDB.userProductDao().insert(up);
                    Log.e("UPDATED PRODUCT UPDATE", String.format("%s doesn't exist but is in update bundle (%s)", up.getName(), up.getUuid()));
                }
            } catch (Exception e){
                // the goal is to catch exceptions that might arise if target row doesn't exist
                e.printStackTrace();
            }
        }
    }

    private static void applyAddedProductsUpdates(AppDB appDB, List<UserProduct> addedProducts) {
        for(UserProduct up : addedProducts){
            try {
                if (appDB.userProductDao().getByUUID(up.getUuid()) == null) {
                    appDB.userProductDao().insert(up);
                } else {
                    Log.e("ADDED PRODUCT UPDATE", String.format("%s already exists (%s)", up.getName(), up.getUuid()));
                }
            } catch (Exception e){
                // the goal is to catch exceptions that might arise if target row doesn't exist
                e.printStackTrace();
            }
        }
    }

    private static List<ProductPrice> unwrapListOfProductPrices(JSONArray pricesList) {
        List<ProductPrice> prices = new ArrayList<>();
        for (int i = 0; i < pricesList.length(); i++) {
            try {
                prices.add(deserializePrice((JSONObject) pricesList.get(i)));

            } catch (JSONException e ){
                e.printStackTrace();
                Log.e("PRICE BUNDLE LOAD", "Price deserialization failed");
            }
        }
        return prices;
    }

    private static ProductPrice deserializePrice(JSONObject json) throws JSONException {
        ProductPrice pp = new ProductPrice(json.getString("uuid"),
                json.getString("store"),
                json.getDouble("price"),
                json.getInt("last_update_timestamp"));
        return pp;
    }

    private static List<UserProduct> unwrapAddedProductsBundle(String userUUID,
                                                        JSONArray addedProductsBundle) {
        // name, store, price
        List<UserProduct> userProducts = new ArrayList<>();
        for (int i = 0; i < addedProductsBundle.length(); i++) {
            try {
                JSONObject newProductJSON = addedProductsBundle.getJSONObject(i);
                UserProduct up = productJsonToProduct(userUUID, newProductJSON, i);
                userProducts.add(up);
            } catch (JSONException e) {
                e.printStackTrace();
                Log.e("ADDED BUNDLE LOAD", "Product deserialization failed");
            }
        }
        return userProducts;
    }

    @NonNull
    private static UserProduct productJsonToProduct(String userUUID, JSONObject productJSON, int i) throws JSONException {
        UserProduct up = new UserProduct(userUUID,
                productJSON.getString("name"),
                productJSON.getString("store"),
                productJSON.getDouble("price"),
                false,
                productJSON.getInt("priority"),
                productJSON.getInt("last_update_timestamp"));
        up.setUuid(productJSON.getString("uuid"));
        return up;
    }

    private static List<UserProduct> unwrapUpdatedProductsBundle(String userUUID, JSONArray updatedProductsBundle){
        // name, store, price
        List<UserProduct> userProducts = new ArrayList<>();
        for (int i = 0; i < updatedProductsBundle.length(); i++) {
            try {
                JSONObject productJSON = updatedProductsBundle.getJSONObject(i);
                UserProduct up = productJsonToProduct(userUUID, productJSON, i);
                userProducts.add(up);
            } catch (JSONException e) {
                e.printStackTrace();
                Log.e("ADDED BUNDLE LOAD", "Product deserialization failed");
            }
        }
        return userProducts;
    }

    private static List<String> unwrapListOfProductsToRemove(JSONArray productsToRemoveUpdateBundle){
        List<String> productsToRemove = new ArrayList<>();
        for (int i = 0; i < productsToRemoveUpdateBundle.length(); i++){
            try {
                productsToRemove.add(productsToRemoveUpdateBundle.getString(i));
            } catch (JSONException e) {
                e.printStackTrace();
                Log.e("REMOVED BUNDLE LOAD", "UUID deserialization failed");
            }
        }
        return productsToRemove;
    }

    private static Map<String, Integer> unwrapListOfQuantityUpdates(JSONObject quantityUpdatesBundle){
        Map<String, Integer> quantityUpdatesMap = new HashMap<>();
        for (Iterator<String> it = quantityUpdatesBundle.keys(); it.hasNext(); ) {
            String key = it.next();
            try {
                quantityUpdatesMap.put(key, quantityUpdatesBundle.getInt(key));
            } catch (JSONException e) {
                e.printStackTrace();
                Log.e("QUANTITY BUNDLE LOAD", "Quantity deserialization failed");
            }
        }
        return quantityUpdatesMap;
    }
}


