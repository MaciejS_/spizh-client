package pg.ium.spizhclient;

import android.content.Context;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

import pg.ium.spizhclient.persistence.AppDB;
import pg.ium.spizhclient.persistence.entity.UserProduct;
import pg.ium.spizhclient.sync.OutgoingSyncPackaging;

import static pg.ium.spizhclient.Utils.assembleAuthorizationHeader;

/**
 * Created by MaciejS_ on 2017-11-19.
 */

public class Server {

    public static final String HTTP = "http";
    private String hostname;
    private int port;

    private String username;
    private String password;

    private Context appContext;
    private RequestQueue requestQueue;
    private Gson gson;

    private String deviceID;

    public Server(String hostname, int port, String username, String password, Context appContext) {
        this.hostname = hostname;
        this.port = port;

        this.username = username;
        this.password = password;

        this.appContext = appContext;

        this.requestQueue = Volley.newRequestQueue(appContext);
        gson = new Gson();

        this.deviceID = Settings.Secure.getString(appContext.getContentResolver(),
                Settings.Secure.ANDROID_ID);
    }

    public void setCredentials(String username, String password){
        this.username = username;
        this.password = password;
    }

    public void fetchAllProducts(Response.Listener<JSONObject> responseHandler, Response.ErrorListener errorHandler) {
        URL url = null;
        try {
            url = new URL(HTTP, hostname, port, "/api/v2.0/product");
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }

        JsonObjectRequest r = new JsonObjectRequest(Request.Method.GET,
                url.toString(),
                new JSONObject(),
                responseHandler,
                errorHandler) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                return assembleAuthorizationHeader(username, password);
            }
        };
        this.requestQueue.add(r);
        Log.d("Products_request", "Products request dispatched");
    }

    public void createProduct(UserProduct product,
                              Response.Listener<JSONObject> responseHandler,
                              Response.ErrorListener errorHandler) {
        URL url = null;
        try {
            url = new URL(HTTP, hostname, port, "/api/v2.0/product");
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }

        Map<String, Object> productAsMap = product.toMap();
        JsonObjectRequest r = new JsonObjectRequest(Request.Method.POST,
                url.toString(),
                new JSONObject(productAsMap),
                responseHandler,
                errorHandler) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                return assembleAuthorizationHeader(username, password);
            }
        };
        requestQueue.add(r);
    }

    private void updateProduct(String productId,
                               String field,
                               int newValue,
                               Response.Listener<JSONObject> responseListener,
                               Response.ErrorListener errorListener) {
        String id = productId;
        URL url = null;
        try {
            url = new URL(HTTP, hostname, port, "/api/v2.0/product/" + id + "/update");
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }

        HashMap<String, Object> map = new HashMap<>();
        map.put(field, newValue);
        map.put("device_id", deviceID);

        JSONObject requestJSON = new JSONObject(map);
        JsonObjectRequest r = new JsonObjectRequest(Request.Method.POST,
                url.toString(), requestJSON, responseListener, errorListener) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                return assembleAuthorizationHeader(username, password);
            }
        };
        this.requestQueue.add(r);
    }

    public void updateProductQuantity(String productId,
                                      int delta,
                                      Response.Listener<JSONObject> responseListener,
                                      Response.ErrorListener errorListener) {
        updateProduct(productId, "delta", delta, responseListener, errorListener);
    }

    public void updateProductPriority(String productId,
                                      int delta,
                                      Response.Listener<JSONObject> responseListener,
                                      Response.ErrorListener errorListener) {
        updateProduct(productId, "priority", delta, responseListener, errorListener);
    }

    public void removeProduct(String productId,
                              Response.Listener responseListener,
                              Response.ErrorListener errorListener) {
        URL url = null;
        try {
            url = new URL(HTTP, hostname, port, "/api/v2.0/product/" + productId);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }

        StringRequest r = new StringRequest(Request.Method.DELETE,
                url.toString(),
                responseListener, errorListener) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                return assembleAuthorizationHeader(username, password);
            }
        };
        this.requestQueue.add(r);
    }

    public void createUser(String username,
                           String password,
                           Response.Listener responseListener,
                           Response.ErrorListener errorListener) {
        URL url = null;
        try {
            url = new URL(HTTP, hostname, port, "/api/v2.0/user");
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }

        Map<String, String> userCredentials = new HashMap<>(2);
        userCredentials.put("name", username);
        userCredentials.put("auth", password);

        JSONObject requestJSON = new JSONObject(userCredentials);
        JsonObjectRequest r = new JsonObjectRequest(Request.Method.POST,
                url.toString(), requestJSON, responseListener, errorListener) {
        };
        this.requestQueue.add(r);
    }

    public void loginUser(final String username,
                          final String password,
                          Response.Listener responseListener,
                          Response.ErrorListener errorListener) {
        URL url = null;
        try {
            url = new URL(HTTP, hostname, port, "/api/v2.0/login");
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }

        StringRequest loginRequest = new StringRequest(Request.Method.GET,
                url.toString(),
                responseListener,
                errorListener) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                return assembleAuthorizationHeader(username, password);
            }
        };
        requestQueue.add(loginRequest);
    }

    public void registerDevice(String userID,
                                Response.Listener<JSONObject> responseListener,
                               Response.ErrorListener errorListener){
        URL url = null;
        try {
            url = new URL(HTTP, hostname, port, "/api/v2.0/user/" + userID + "/device");
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }

        Map<String, String> json = new HashMap<>();
        json.put("device_id", deviceID);

        JSONObject requestJSON = new JSONObject(json);
        JsonObjectRequest deviceRegisterRequest = new JsonObjectRequest(Request.Method.POST,
                url.toString(),
                requestJSON,
                responseListener,
                errorListener) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                return assembleAuthorizationHeader(username, password);
            }
        };
        requestQueue.add(deviceRegisterRequest);
    }

    public void sendSyncRequest(final AppDB appDB,
                                String userID,
                                Response.Listener<JSONObject> responseListener,
                                Response.ErrorListener errorListener){
        JSONArray products_update = OutgoingSyncPackaging.assembleAddedProductsUpdate(appDB, userID);
        JSONObject qty_updates = OutgoingSyncPackaging.assembleQuantitiesUpdate(appDB, userID);
        JSONArray removed_products = OutgoingSyncPackaging.assembleRemovedProductsBundle(appDB, userID);
        JSONArray updated_products = OutgoingSyncPackaging.assembleUpdatedProductsUpdate(appDB, userID);
        JSONArray prices = OutgoingSyncPackaging.assemblePricesUpdate(appDB, userID);

        JSONObject update_bundle = assembleUpdatesBundle(products_update,
                qty_updates,
                removed_products,
                updated_products,
                prices);

        URL url = null;
        try {
            url = new URL(HTTP, hostname, port, "/api/v2.0/sync");
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }

        JsonObjectRequest syncRequest = new JsonObjectRequest(Request.Method.POST,
                url.toString(),
                update_bundle,
                responseListener,
                errorListener) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                return assembleAuthorizationHeader(username, password);
            }
        };
        requestQueue.add(syncRequest);
    }

    @NonNull
    private JSONObject assembleUpdatesBundle(JSONArray addedProductsBundle,
                                             JSONObject quantitiesBundle,
                                             JSONArray removedProductIDsBundle,
                                             JSONArray updatedProductsBundle,
                                             JSONArray pricesBundle){
        JSONObject mainBundle = new JSONObject();
        JSONObject productUpdatesBundle = new JSONObject();
        try {
            productUpdatesBundle.put("added", addedProductsBundle);
            productUpdatesBundle.put("updated", updatedProductsBundle);
            productUpdatesBundle.put("removed", removedProductIDsBundle);
            mainBundle.put("product", productUpdatesBundle);
            mainBundle.put("quantity", quantitiesBundle);
            mainBundle.put("price", pricesBundle);
            mainBundle.put("device_id", deviceID);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return mainBundle;
    }


}
