package pg.ium.spizhclient;

import android.util.Base64;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by MaciejS_ on 2017-11-13.
 */

class Utils {
    public static Map<String, String> assembleAuthorizationHeader(String username, String password) {
        Map<String, String> headers = new HashMap<>();
        String credentials = username + ":" + password;
        String auth = "Basic " + Base64.encodeToString(credentials.getBytes(), Base64.NO_WRAP);
        headers.put("Content-Type", "application/json");
        headers.put("Authorization", auth);
        return headers;
    }
}
