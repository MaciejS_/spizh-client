package pg.ium.spizhclient;

import android.app.AlertDialog;
import android.app.ListActivity;
import android.arch.persistence.room.Room;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import pg.ium.spizhclient.persistence.AppDB;
import pg.ium.spizhclient.persistence.Migrations;
import pg.ium.spizhclient.persistence.entity.ProductPrice;
import pg.ium.spizhclient.persistence.entity.Quantity;
import pg.ium.spizhclient.persistence.entity.UserProduct;
import pg.ium.spizhclient.sync.IncomingSyncLogic;

import static pg.ium.spizhclient.persistence.Migrations.migrate10to11;
import static pg.ium.spizhclient.persistence.Migrations.migrate11to12;
import static pg.ium.spizhclient.persistence.Migrations.migrate8to9;
import static pg.ium.spizhclient.persistence.Migrations.migrate9to10;

public class ProductListActivity extends ListActivity {

    public static final int DEFAULT_PRODUCT_PRIORITY = 0;
    private ProductListAdapter adapter;
    private Server server;

    private AppDB appDb;
    private String userUUID = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_list);

        // ListView needs to go here somewhere I guess
        // also, I'll need to set the adapter for product item

        // 1. ListView might not be initialized
        // 2. Fetching products from server should be moved to an AsyncTask

        Log.d("PLA", "entered onCreate");

        Intent intent = getIntent();
        username = intent.getStringExtra("username");
        password = intent.getStringExtra("password");
        userUUID = intent.getStringExtra("user_id");

        productsMap = new HashMap<>();

        appDb = Room.databaseBuilder(getApplicationContext(), AppDB.class, "spizh.db")
                .allowMainThreadQueries()
                .addMigrations(Migrations.migrate5to6, Migrations.migrate6to7, Migrations.migrate7to8, migrate8to9, migrate9to10, migrate10to11, migrate11to12)
                .build();

        this.server = new Server(getString(R.string.server_ip_addr), 7447, username, password, getApplicationContext());

        Log.d("PLA", "calling fetchAllProducts");
//        List<UserProduct> allProductsForUser = appDb.userProductDao().getAllForUser(userUUID);
        inspectDB();
        productsMap = loadProductsMap(userUUID, true);
        refreshContentAdapter(productsMap);

        FloatingActionButton addButton = (FloatingActionButton) findViewById(R.id.addButton);
        addButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                buildAddProductDialog().show();
            }
        });

        FloatingActionButton syncButton = (FloatingActionButton) findViewById(R.id.syncButton);
        syncButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                runSync();
            }
        });
    }

    private void inspectDB() {
        Log.i("DB INSPECT", userUUID);
        List<UserProduct> allForUser = appDb.userProductDao().getAllForUser(userUUID);
        for (UserProduct up : allForUser) {
            Log.i("DB INSPECT PRODUCT", up.toString());
        }
        List<Quantity> allqutys = appDb.quantityDao().getAll();
        for (Quantity q : allqutys) {
            Log.i("DB INSPECT QTY", q.toString());
        }
    }

    private void runSync() {
        server.sendSyncRequest(appDb,
                userUUID,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        IncomingSyncLogic.applyUpdate(response, appDb, userUUID);
                        refreshContentAdapter(loadProductsMap(userUUID, true));
                        Toast.makeText(ProductListActivity.this, "Sync complete", Toast.LENGTH_LONG).show();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(ProductListActivity.this, "Server unavailable", Toast.LENGTH_LONG).show();
                    }
                });
    }

    private AlertDialog buildAddProductDialog() {
        LayoutInflater popupLayoutInflater = (LayoutInflater) getApplicationContext()
                .getSystemService(LAYOUT_INFLATER_SERVICE);
        final View addProductDialogView = popupLayoutInflater.inflate(R.layout.new_product_dialog, null);

        AlertDialog.Builder b = new AlertDialog.Builder(ProductListActivity.this)
                .setTitle(R.string.add_product_dialog_title)
                .setView(addProductDialogView)
                .setCancelable(false)
                .setPositiveButton("Add", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        UserProduct userProduct = createProductFromPopupViewContents(addProductDialogView);
                        // TODO: I could deprecate send-throughs altogether eventually
                        sendNewProductToServer(userProduct);
                        productsMap.put(userProduct.getUuid(), conjureDisplayableProduct(userProduct.getUuid()));
                        refreshContentAdapter(productsMap);
                    }
                }).setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
        return b.create();
    }

    private Map<String, Product> loadProductsMap(String userUUID, boolean visibleOnly) {
        List<UserProduct> products = appDb.userProductDao().getAllForUser(userUUID);
        Map<String, Product> productsMap = new HashMap<>();
        for (UserProduct up : products) {
            Log.i("PRODUCT", up.getName() + " " + String.valueOf(up.isRemoved()));
            if (!visibleOnly || isVisible(up)) {
                productsMap.put(up.getUuid(), conjureDisplayableProduct(up.getUuid()));
            }
        }
        return productsMap;
    }

    private boolean isVisible(UserProduct up) {
        return !up.isRemoved();
    }

    private void refreshContentAdapter(Map<String, Product> productMap) {
        Log.d("PLA", "setting up content adapter");

        // TODO: Add some default sorting order, so that the list wouldn't end up randomized
        Product[] products = Arrays.copyOf(productMap.values().toArray(), productMap.size(), Product[].class);
        List<Product> productList = new ArrayList<>(productMap.values());
        Collections.sort(productList, new Comparator<Product>() {
            @Override
            public int compare(Product o1, Product o2) {
                return o1.getName().compareTo(o2.getName());
            }
        });
        adapter = new ProductListAdapter(this, productList, server, appDb);
        setListAdapter(adapter);
    }

    private void sendNewProductToServer(final UserProduct product) {
        server.createProduct(product,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
//                        String productIdKey = getString(R.string.product_id_key_in_add_repsonse);
                        Log.i("ADD NEW PRODUCT", "Product \"" + product.getName() + "\" sent to server");
                        product.setLocalOnly(false);
                        appDb.userProductDao().update(product);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.i("ADD NEW PRODUCT", "Server is unreachable");
                    }
                });
    }

    @Override
    protected void onListItemClick(ListView l, View v, int position, long id) {
        super.onListItemClick(l, v, position, id);
        String clickedProduct = getListView().getItemAtPosition(position).toString();
        Log.d("ProductListView", "Clicked on: " + clickedProduct);
    }

    private UserProduct createProductFromPopupViewContents(View popupView) {
        EditText nameEdit = (EditText) popupView.findViewById(R.id.new_product_name);
        EditText storeEdit = (EditText) popupView.findViewById(R.id.new_product_store);
        EditText priceEdit = (EditText) popupView.findViewById(R.id.new_product_price);

        String productName = nameEdit.getText().toString();
        String storeName = storeEdit.getText().toString();
        Double price = Double.valueOf(priceEdit.getText().toString());

        int currentTimestamp = (int)(System.currentTimeMillis() / 1000L);

        UserProduct newUserProduct = new UserProduct(userUUID,
                productName,
                storeName,
                price,
                true,
                DEFAULT_PRODUCT_PRIORITY,
                currentTimestamp
                );
        Quantity quantity = new Quantity(newUserProduct.getUuid(), 0, 0);
        this.appDb.userProductDao().insert(newUserProduct);
        this.appDb.quantityDao().insert(quantity);

        return newUserProduct;
    }

    private Product conjureDisplayableProduct(String productUUID) {
        UserProduct up = this.appDb.userProductDao().getByUUID(productUUID);
        Quantity q = this.appDb.quantityDao().getQuantityForProduct(productUUID);
        List<ProductPrice> prices = this.appDb.productPriceDao().getVisiblePricesForProduct(productUUID);
        int qty;
        if (q == null) {
            qty = 0;
        } else {
            qty = q.getTotalQuantity();
        }
        Product product = new Product(up.getUuid(),
                up.getName(),
                qty,
                up.getPriority(),
                prices);

        return product;
    }

    private String username;
    private String password;

    private Map<String, Product> productsMap;
    private Product[] productArray;
}
