package pg.ium.spizhclient;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Adapter;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.NumberPicker;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;

import java.text.NumberFormat;
import java.util.List;

import pg.ium.spizhclient.persistence.AppDB;
import pg.ium.spizhclient.persistence.entity.ProductPrice;
import pg.ium.spizhclient.persistence.entity.Quantity;
import pg.ium.spizhclient.persistence.entity.UserProduct;

/**
 * Created by MaciejS_ on 2017-11-13.
 */

public class ProductListAdapter extends ArrayAdapter<Product> {
    private final Context context;
    private final List<Product> values;
    private Server serverHandle;
    private AppDB appDBHandle;

    public ProductListAdapter(Context context, List<Product> values, Server serverHandle, AppDB appDBHandle) {
        super(context, -1, values);
        this.context = context;
        this.values = values;
        this.serverHandle = serverHandle;
        this.appDBHandle = appDBHandle;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final Product product = values.get(position);

        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rowView = inflater.inflate(R.layout.product_list_entry, parent, false);
        rowView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d("Product clicked", product.toString());
            }
        });
        rowView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                AlertDialog removeDialog = buildRemoveProductAlertDialog(product);
                removeDialog.show();
                return true;
            }
        });

        TextView nameView = (TextView) rowView.findViewById(R.id.product_name);
        TextView storeView = (TextView) rowView.findViewById(R.id.product_store_info);
        TextView priceView = (TextView) rowView.findViewById(R.id.price_info);
        TextView priorityView = (TextView) rowView.findViewById(R.id.priority_info);
        final TextView quantityView = (TextView) rowView.findViewById(R.id.product_quantity);

        nameView.setText(product.getName());
        priorityView.setText("Priority: " + String.valueOf(product.getPriority()));
        priorityView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog d = buildChangePriorityDialog(product);
                d.show();
            }
        });


        NumberFormat currencyFormatter = NumberFormat.getCurrencyInstance();
        ProductPrice defaultProductPrice = product.getPrices().get(0);
        priceView.setText(currencyFormatter.format(defaultProductPrice.getPrice()));
        storeView.setText(defaultProductPrice.getStore());

        quantityView.setText(Integer.toString(product.getQuantity()));

        TextView plusButton = (TextView) rowView.findViewById(R.id.plus_button);
        plusButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog dialog = buildChangeQtyDialog(product, "Add");
                dialog.show();
            }
        });

        TextView minusButton = (TextView) rowView.findViewById(R.id.minus_button);
        minusButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog dialog = buildChangeQtyDialog(product, "Remove");
                dialog.show();
            }
        });

        LinearLayout price_info_group = (LinearLayout) rowView.findViewById(R.id.price_info_group);
        price_info_group.setOnClickListener(new LinearLayout.OnClickListener(){
            @Override
            public void onClick(View v) {
                AlertDialog dialog = buildPriceListDialog(product);
                dialog.show();
            }
        });

        return rowView;
    }

    private AlertDialog buildChangeQtyDialog(final Product product, String action) {
        LayoutInflater popupLayoutInflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final View addUserDialogView = popupLayoutInflater.inflate(R.layout.change_product_qty_popup, null);

        final NumberPicker np = (NumberPicker) addUserDialogView.findViewById(R.id.numberPicker1);
        np.setMaxValue(100);
        np.setMinValue(0);
        np.setWrapSelectorWheel(false);

        final int modifier = action.equals("Remove") ? -1 : 1;

        AlertDialog.Builder b = new AlertDialog.Builder(context)
                .setTitle(action + " items")
                .setView(addUserDialogView)
                .setCancelable(false)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        int delta = modifier * np.getValue();
                        updateProductQuantity(product, delta);

                        serverHandle.updateProductQuantity(product.getId(), delta,
                                new Response.Listener() {
                                    @Override
                                    public void onResponse(Object response) {
                                        Toast.makeText(context, "Quantity updated online.", Toast.LENGTH_SHORT).show();
                                    }
                                },
                                errorListener);
                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
        final AlertDialog dialog = b.create();
        return dialog;
    }

    private AlertDialog buildChangePriorityDialog(final Product product) {
        LayoutInflater popupLayoutInflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final View addUserDialogView = popupLayoutInflater.inflate(R.layout.change_product_qty_popup, null);

        final NumberPicker np = (NumberPicker) addUserDialogView.findViewById(R.id.numberPicker1);
        np.setMaxValue(100);
        np.setMinValue(0);
        np.setWrapSelectorWheel(false);

        AlertDialog.Builder b = new AlertDialog.Builder(context)
                .setTitle("Change priority")
                .setView(addUserDialogView)
                .setCancelable(false)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        int newPriority = np.getValue();
                        updateProductPriority(product, newPriority);

                        serverHandle.updateProductPriority(product.getId(), newPriority,
                                new Response.Listener() {
                                    @Override
                                    public void onResponse(Object response) {
                                        Toast.makeText(context, "Priority updated online.", Toast.LENGTH_SHORT).show();
                                    }
                                },
                                errorListener);
                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
        final AlertDialog dialog = b.create();
        return dialog;
    }

    private AlertDialog buildPriceListDialog(final Product product) {
        LayoutInflater popupLayoutInflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final View productListPopupView = popupLayoutInflater.inflate(R.layout.price_list_popup, null);


        AlertDialog.Builder b = new AlertDialog.Builder(context)
                .setTitle("Alternative prices")
                .setView(productListPopupView)
                .setCancelable(true)
                .setPositiveButton("Add", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        AlertDialog addPriceAlertDialog = buildAddPriceDialog(product);
                        addPriceAlertDialog.show();
                    }
                }).setNegativeButton("Close", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
        ListView lv = (ListView) productListPopupView.findViewById(R.id.prices_list);
        ListAdapter adapter = new PriceListAdapter(context, product.getPrices(), appDBHandle);
        lv.setAdapter(adapter);

        final AlertDialog dialog = b.create();
        return dialog;
    }

    private AlertDialog buildAddPriceDialog(final Product product) {
        LayoutInflater popupLayoutInflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final View newPriceDialog = popupLayoutInflater.inflate(R.layout.new_price_dialog, null);

        AlertDialog.Builder b = new AlertDialog.Builder(context)
                .setTitle("Add price for " + product.getName())
                .setView(newPriceDialog)
                .setCancelable(true)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        ProductPrice price = createPriceFromPopupViewContents(product.getId(), newPriceDialog, appDBHandle);
                        if (price != null) {
                            // product not revived, insert received price
                            appDBHandle.productPriceDao().insert(price);
                            product.getPrices().add(price);
                        }
                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener(){
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
        final AlertDialog dialog = b.create();
        return dialog;
    }

    private static ProductPrice createPriceFromPopupViewContents(String productUUID, View popupView, AppDB appDB) {
        EditText storeEdit = (EditText) popupView.findViewById(R.id.new_price_store_name);
        EditText priceEdit = (EditText) popupView.findViewById(R.id.new_price_value);

        String storeName = storeEdit.getText().toString();
        Double newPrice = Double.valueOf(priceEdit.getText().toString());
        int currentTimestamp = (int) (System.currentTimeMillis() / 1000L);

        for (ProductPrice p: appDB.productPriceDao().getPricesForProduct(productUUID)){
            if ((p.getRemoved() == true) && (p.getStore() == storeName)){
                p.revive();
                p.setPrice(newPrice);
                appDB.productPriceDao().update(p);
                return null;
            }
        }

        return new ProductPrice(null, productUUID, storeName, newPrice, currentTimestamp);
    }

    private void updateProductQuantity(Product product, int delta) {
        product.setQuantity(product.getQuantity() + delta);
        ProductListAdapter.this.notifyDataSetChanged();

        Quantity q = appDBHandle.quantityDao().getQuantityForProduct(product.getId());
        q.updateLocalQuantity(delta);
        appDBHandle.quantityDao().update(q);
    }

    private void updateProductPriority(Product product, int newPriority){
        product.setPriority(newPriority);
        ProductListAdapter.this.notifyDataSetChanged();

        UserProduct up = appDBHandle.userProductDao().getByUUID(product.getId());
        up.setPriority(newPriority);
        up.bumpLastUpdateTime();
        appDBHandle.userProductDao().update(up);
    }

    @Override
    public void notifyDataSetChanged() {
        super.notifyDataSetChanged();
    }


    private AlertDialog buildRemoveProductAlertDialog(final Product product) {
        return new AlertDialog.Builder(context)
                .setTitle("Remove product?")
                .setCancelable(false)
                .setMessage("Do you want to remove " + product.getName() + " from the list?")
                .setPositiveButton("Remove", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog1, int which) {
                        values.remove(product);
                        UserProduct up = appDBHandle.userProductDao().getByUUID(product.getId());
                        up.setRemoved(true);
                        appDBHandle.userProductDao().update(up);
                        ProductListAdapter.this.notifyDataSetChanged();

                        serverHandle.removeProduct(product.getId(),
                                new Response.Listener() {
                                    @Override
                                    public void onResponse(Object response) {
                                        Toast.makeText(context, "Product removed.", Toast.LENGTH_SHORT).show();
                                    }
                                }, new Response.ErrorListener() {
                                    @Override
                                    public void onErrorResponse(VolleyError error) {
                                        Toast.makeText(context, "Product removed offline.", Toast.LENGTH_SHORT).show();
                                    }
                                }
                        );
                    }
                }).setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog1, int which) {
                        dialog1.dismiss();
                    }
                }).create();
    }

    private final Response.ErrorListener errorListener = new Response.ErrorListener() {
        @Override
        public void onErrorResponse(VolleyError error) {
            Toast.makeText(context, "Quantity updated offline.", Toast.LENGTH_SHORT).show();
        }
    };

}
