package pg.ium.spizhclient;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;

import java.text.NumberFormat;
import java.util.List;
import java.util.Objects;

import pg.ium.spizhclient.persistence.AppDB;
import pg.ium.spizhclient.persistence.entity.ProductPrice;
import pg.ium.spizhclient.persistence.entity.UserProduct;

/**
 * Created by MaciejS_ on 2018-01-15.
 */

public class PriceListAdapter extends ArrayAdapter<ProductPrice> {
    private final Context context;
    private final List<ProductPrice> values;
    private AppDB appDBHandle;

    public PriceListAdapter(Context context, List<ProductPrice> values, AppDB appDBHandle){
        super(context, -1, values);
        this.context = context;
        this.values = values;
        this.appDBHandle = appDBHandle;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
//        return super.getView(position, convertView, parent);
        final ProductPrice price = values.get(position);

        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rowView = inflater.inflate(R.layout.price_list_entry, parent, false);
        rowView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d("Product clicked", price.toString());
            }
        });
        rowView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                AlertDialog removeDialog = buildRemovePriceAlertDialog(price);
                removeDialog.show();
                return true;
            }
        });

        TextView storeView = (TextView) rowView.findViewById(R.id.product_store_info);
        TextView priceView = (TextView) rowView.findViewById(R.id.price_info);

        NumberFormat currencyFormatter = NumberFormat.getCurrencyInstance();
        priceView.setText(currencyFormatter.format(price.getPrice()));
        storeView.setText(price.getStore());

        priceView.setOnClickListener(new TextView.OnClickListener(){
            @Override
            public void onClick(View v) {
                buildModifyPriceDialog(price).show();
            }
        });

//        rowView.setOnLongClickListener(View.OnLongClickListener);

        // change price dialog
        return rowView;
    }

    private AlertDialog buildModifyPriceDialog(final ProductPrice price) {
        LayoutInflater popupLayoutInflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final View changePriceDialog = popupLayoutInflater.inflate(R.layout.change_price_dialog, null);


        AlertDialog.Builder b = new AlertDialog.Builder(context)
                .setTitle("Change price at " + price.getStore())
                .setView(changePriceDialog)
                .setCancelable(true)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        EditText priceEdit = (EditText) changePriceDialog.findViewById(R.id.change_price_value);
                        Double newPrice = Double.valueOf(priceEdit.getText().toString());
                        price.setPrice(newPrice);
                        price.setLastUpdateTimestamp((int)(System.currentTimeMillis() / 1000L));
                        appDBHandle.productPriceDao().update(price);

                        UserProduct up = appDBHandle.userProductDao().getByUUID(price.getUserProductUUID());
                        if (Objects.equals(price.getStore(), up.getStore())){
                            // update main price
                            up.setPrice(newPrice);
                        }
                        PriceListAdapter.this.notifyDataSetChanged();
                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener(){
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
        final AlertDialog dialog = b.create();
        return dialog;
    }

    private AlertDialog buildRemovePriceAlertDialog(final ProductPrice price) {
        return new AlertDialog.Builder(context)
                .setTitle("Remove product?")
                .setCancelable(false)
                .setMessage("Do you want to remove " + price.getStore() + " price?")
                .setPositiveButton("Remove", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog1, int which) {
                        values.remove(price);
                        price.setRemoved(true);
                        price.setLastUpdateTimestamp((int)(System.currentTimeMillis() / 1000L));
                        appDBHandle.productPriceDao().update(price);
                        PriceListAdapter.this.notifyDataSetChanged();
                    }
                }).setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog1, int which) {
                        dialog1.dismiss();
                    }
                }).create();
    }

//    private AlertDialog buildRemoveProductAlertDialog(final Price price) {
//        return new AlertDialog.Builder(context)
//                .setTitle("Remove product?")
//                .setCancelable(false)
//                .setMessage("Do you want to remove " + product.getName() + " from the list?")
//                .setPositiveButton("Remove", new DialogInterface.OnClickListener() {
//                    @Override
//                    public void onClick(DialogInterface dialog1, int which) {
//                        values.remove(price);
//                        appDBHandle.userProductDao().update(up);
//                        PriceListAdapter.this.notifyDataSetChanged();
//
//                        // actually remove the price from db
//
//                    }
//                }).setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
//                    @Override
//                    public void onClick(DialogInterface dialog1, int which) {
//                        dialog1.dismiss();
//                    }
//                }).create();
//    }
}
